package com.example.cursopucmm;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity /*implements View.OnClickListener*/ {

    TextView pantalla;
    EditText nombre;

    static double dolar = 58.5;
    static double euro = 64;
    boolean button_status = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pantalla = findViewById(R.id.pantalla);
        nombre = findViewById(R.id.nombre);

        fx_change_buttons();


        nombre.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s.toString().trim())) {
                    button_status = false;

                } else {
                    button_status = true;
                }

                fx_change_buttons();

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    public void fx_change_buttons() {

        ViewGroup layout = (ViewGroup) findViewById(R.id.calc_grid);

        for (int i = 0; i < layout.getChildCount(); i++) {

            View child = layout.getChildAt(i);
            if (child instanceof Button) {
                Button button = (Button) child;
                button.setEnabled(button_status);
            }

        }

    }

    public void fx_add_to_screen(View v) {

        String buttonText = ((Button) v).getText().toString();

        if (buttonText != "0") {
            pantalla.append((buttonText));
        } else {
            if (pantalla.getText() != "")
                pantalla.append((buttonText));
        }
    }

    public void fx_currency_change(View v) {
        try {

              if (!TextUtils.isEmpty(pantalla.getText().toString().trim())) {
                double cantidad = Double.parseDouble(pantalla.getText().toString());
                  double resultado = 0;
                  String buttonText = ((Button) v).getText().toString();
                  double factor=0;

                  switch(buttonText) {
                      case "RD$→$":
                          resultado = cantidad / dolar;
                          break;
                      case "$→RD$":
                          resultado = cantidad * dolar;
                          break;
                      case "€→RD$":
                          resultado = cantidad * euro;
                          break;
                      case "RD$→€":
                          resultado = cantidad / euro;
                          break;
                      default:
                          // code block
                  }
                  pantalla.setText(String.format("%.2f", resultado));
            }
        } catch (Exception e) {

            System.out.println("Error " + e.getMessage());
        }
    }
    public void fx_clear_screen(View v) {
        pantalla.setText("");
    }
}
